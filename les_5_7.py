from random import randint


n = int(input())
count = 0
while True:
    c = randint(0, 10)
    if c == n:
        print('You are the winner')
        break
    elif c != n:
        count += 1
        k = int(input())
        if c == k:
            print('You are the winner')
            break
        elif c != k:
            count += 1
            b = int(input())
            if c == b:
                print('You are the winner')
                break
            elif c != b:
                count += 1
                if count >= 3:
                    print('You are the loser')
                    break